\documentclass[11pt,a4paper]{article}
\usepackage[utf8x]{inputenc}
\usepackage{ucs}
\usepackage[T1]{fontenc}
\usepackage[bookmarks,pdfborder={0 0 0}]{hyperref}
\usepackage[left=3cm,right=3cm,top=3.5cm,bottom=3.5cm]{geometry}
\usepackage[italian]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{icomma}
\usepackage{cancel}
\usepackage{lscape}
\usepackage{longtable}
\usepackage[space]{grffile}
\usepackage[amssymb,italian]{SIunits}
\usepackage[version=3]{mhchem}
\graphicspath{{./images/}}
\author{Matteo Italia}
\title{Convezione solutale Ludox TMA - report}
\begin{document}
\maketitle

\section{Setup sperimentale}

L'apparato sperimentale è stato impostato con il fine di ottenere misure shadowgrafiche continuative di una cella inclinata contenente una miscela binaria in convezione solutale a differenza di temperatura controllata.

Per chiarezza espositiva conviene dividere l'apparato sperimentale in cella, sezione ottica e sezione elettronica/informatica.

\subsection{Cella}

La cella è costituita da due lastre circolari di zaffiro (trasparente e dalla elevata conducibilità termica) dello spessore di \unita{2}{\centi\meter}\footnote{Vado a memoria, da verificare.}, tra cui sono stretti un o-ring di gomma e degli spessori di altezza fissata (nel nostro caso \unita{2,9}{\milli\meter}); il campione da esaminare è iniettato con una siringa nello spazio delimitato dalle lastre (sopra e sotto) e dall'o-ring (lateralmente) (figura \ref{fig:riempimento cella}).

\begin{figure}
\includegraphics[width=\textwidth/21*10]{riempimento cella siringa}
\includegraphics[width=\textwidth/21*10]{riempimento cella dettaglio}
\caption{Riempimento della ``cella interna'' tramite siringa.}
\label{fig:riempimento cella}
\end{figure}


La ``cella interna'' è quindi posta tra due elementi costituiti da una cella di Peltier circolare (forata al centro) e da una cavità per la circolazione del liquido di termostatazione. Tra una faccia del Peltier e il vetro è posto un sottile strato di pasta termoconduttiva a base siliconica (figura \ref{fig:peltier e cella montata}) per massimizzare l'efficienza di conduzione termica tra Peltier e vetro. Tutti i fissaggi sono effettuati con chiave dinamometrica, in modo da ottenere una pressione uniforme tra i vari elementi (e quindi non introdurre inclinazioni asimmetriche nel sistema o una distribuzione non uniforme della pasta termoconduttiva).

La cella completa è quindi fissata ad un supporto ad inclinazione variabile. Una volta messo perfettamente in piano il supporto (tramite livella a bolla ad elevata sensibilità), agendo su una singola vite si può modificare facilmente l'inclinazione.

La geometria del supporto consente una semplice determinazione dell'angolo di inclinazione in base all'innalzamento di un punto del supporto letto tramite un comparatore (precisione \unita{10}{\micro\meter}; figura \ref{fig:peltier e cella montata}).

Il supporto infatti appoggia in maniera stabile sulle viti dei punti $A$ e $B$ (in figura \ref{fig:geometria supporto}) e si agisce sul punto $C$ per variare l'inclinazione; se si va a misurare l'innalzamento $h$ (rispetto alla condizione in cui è perfettamente in piano) del supporto in un punto qualunque della retta parallela a $\overline{AB}$ passante per $C$, è facile verificare che l'angolo di inclinazione $\vartheta$ del supporto risulta pari a $\vartheta = \arctan{(h/R)} \approx h/R$.

In generale, nel resto del report le inclinazioni sono date come {\milli\meter} letti sul comparatore; è utile quindi ricordare che \unita{1}{\milli\meter} di innalzamento corrisponde\footnote{In approssimazione di piccoli angoli, perfettamente adeguata al caso in esame, dato che per gli angoli di nostro interesse introduce generalmente un errore inferiore alla precisione strumentale del comparatore.} ad un'inclinazione del supporto di $\unita{6,\overline{6}}{\milli\radian} \approx \unita{0,38}{\degree} \approx \unita{22,92}{\arcminute}$.

\begin{figure}
\includegraphics[width=\textwidth/21*10]{peltier}
\includegraphics[width=\textwidth/21*10]{cella montata}
\caption{Elemento Peltier con pasta termoconduttiva, cella montata.}
\label{fig:peltier e cella montata}
\end{figure}


\begin{figure}
\begin{center}
\includegraphics[width=10cm]{geometria supporto}
\end{center}
\caption{Geometria del supporto ad inclinazione variabile.}
\label{fig:geometria supporto}
\end{figure}

\subsection{Ottica}

La parte ottica del sistema è costituita da un LED ad elevata luminosità, una coppia di lenti e una videocamera a CCD collegata al computer.

La luce (non coerente) emessa dal LED arriva sulla prima lente (\unita{f=50}{\centi\meter})\footnote{Inizialmente si era usata una lente a focale più corta per questioni di spazio, ma si è passati ad una lente a focale più lunga per sfruttare solo la parte centrale del fascio del LED; in questa maniera l'illuminazione risulta più omogenea ed è possibile impostare al meglio l'esposizione del CCD.}, posizionata con precisione alla distanza focale, in modo da rendere paralleli i raggi divergenti emessi dal LED. Il fascio prosegue nella cella, dove attraversa il campione e prosegue sulla lente successiva, che forma l'immagine del piano della cella (e delle ombre dovute alle perturbazioni del campione) sul CCD della telecamera (la distanza tra cella e lente e tra lente e CCD è stata determinata empiricamente).

Il fatto che i raggi che attraversano il campione sono paralleli garantisce che l'immagine ottenuta è influenzata esclusivamente da gradienti orizzontali di indice di rifrazione, permettendo quindi di vedere come macchie chiare o scure sull'immagine finale le zone di flusso di liquido caldo o freddo. Esse infatti hanno indice di rifrazione diverso (più alto nel caso delle zone fredde, più basso per quelle calde), e quindi agiscono complessivamente come lenti, facendo convergere o divergere i raggi (vedi figura \ref{fig:shadowgrafia}).

\begin{figure}[t!]
\centerline{\includegraphics[height=7cm, angle=270]{shadowgraph}}
\caption{Schema di funzionamento della shadowgrafia; le zone calde e fredde stabiliscono un gradiente di indice di rifrazione, e agiscono quindi come lenti sui raggi incidenti, determinando così le zone chiare e scure sull'immagine acquisita.}
\label{fig:shadowgrafia}
\end{figure}


\subsection{Elettronica/informatica}

Ogni lato della cella dispone di due termistori (uno da \unita{100}{\kilo\ohm} e uno da \unita{10}{\kilo\ohm}) per la misura della temperatura; i termistori di resistenza maggiore sono stati collegati ai multimetri, mentre gli altri ad una scheda di acquisizione (NI 4350). L'alimentazione dei Peltier, a sua volta, è controllata da una coppia di alimentatori, collegati come i multimetri al PC tramite interfaccia GPIB.

Un programma Python in esecuzione sul computer legge continuamente i dati dei termistori da \unita{100}{\kilo\ohm} e determina tramite controller PID la correzione da apportare all'alimentazione dei due Peltier.\footnote{I termistori da \unita{100}{\kilo\ohm} sono in proporzione meno influenzati dalla resistenza dei fili di collegamento, per cui sono quelli impiegati effettivamente per la rilevazione della temperatura da usare nel controller PID; per la misura della resistenza vengono usati i multimetri in quanto più veloci del NI 4350, che viene usato solo per misure ausiliarie.}

In parallelo, un thread secondario ogni \unita{5}{\second} provvede ad acquisire un'immagine dal CCD, a cui viene sottratto il background acquisito ad inizio misura prima di essere memorizzata su disco assieme a data e ora di acquisizione e temperature dei due lati della cella. Da questi dati è poi possibile generare (tramite un altro script) un grafico dell'andamento del contrasto e della temperatura nel tempo (vedi ad esempio figura \ref{fig:grafico contrasto misure inclinazione intermedia}).

Un terzo thread effettua ogni 10 secondi una misurazione indipendente delle temperature da tutti i quattro termistori (per aggirare eventuali problemi ai due termistori principali), e se i parametri sono fuori range (\unita{5-70}{\celsius}) interrompe la misura onde evitare di danneggiare campione e strumentazione.

\subsection{Campione utilizzato}

In tutte le misurazioni si è usato come campione il Ludox TMA in soluzione in acqua distillata con concentrazione massa/massa del 4\%; il campione in questione è una sospensione colloidale di sferette micrometriche di \ce{SiO2}, che presenta le proprietà fluidodinamiche riportate in tabella \ref{tab:proprieta ludox}.

\begin{table}
\begin{center}\begin{tabular}{lrr|lrr}
\toprule
$\alpha$ & $3,03 \cdot 10^{-04}$ & \unita{}{\kelvin^{-1}} & $D_T$ & $1,48 \cdot 10^{-03}$ & \unita{}{{\centi\meter}^2} \\ 
$\beta$ & $0,57$ & & $D$ & $2,20 \cdot 10^{-07}$ & \unita{}{{\centi\meter}^2\per\second} \\ 
$\nu$ & $8,18 \cdot 10^{-03}$ & \unita{}{{\centi\meter}^2\per\second} & $S_T$ & -0,047 & \unita{}{\kelvin^{-1}} \\ 
\bottomrule
\end{tabular}
\end{center}
\caption{Proprietà fluidodinamiche campione Ludox TMA}
\label{tab:proprieta ludox}
\end{table}

Dal momento che le sferette di \ce{SiO2} sono termofile, per attivarne la convezione è necessario regolare i Peltier per scaldare la piastra superiore e raffreddare quella inferiore; dato che invece la convezione dell'acqua si ha nella configurazione opposta, questo ha anche l'effetto di consentirci di osservare esclusivamente la convezione del soluto, senza che questa sia influenzata in maniera significativa dalla convezione del solvente.

\section{Dati acquisiti}

Sono state effettuate finora complessivamente 31 prese dati utilizzabili, di cui 7 ad inclinazione nulla, 4 con inclinazione in un intorno di \unita{3}{\milli\meter} e 12 ad inclinazione massima (\unita{10}{\milli\meter}). Si è insistito particolarmente sulle misure a massima inclinazione per il comportamento peculiare osservato.

Per ogni misura si è provveduto a mantenere la differenza di temperatura costante (normalmente con \unita{\sigma \leq 0.05}{\kelvin} una volta stabilizzata la temperatura) per \unita{55000 - 90000}{\second} o più (a seconda del dataset).

In generale, le immagini acquisite con \unita{\Delta T \leq 2.5}{\kelvin} sono risultate troppo poco contrastate per essere utilizzabili, probabilmente per per limiti del setup sperimentale.

In base ai dati di campione e cella si è determinato che il tempo di diffusione del soluto nella cella è nell'ordine delle 10 ore, ed è stato quindi preso come intervallo da porre tra una misura e l'altra per iniziare sempre l'esperimento con il sistema nello stesso stato di diffusione uniforme del soluto. In ogni caso, alcune misure suggeriscono che comunque questo possa non essere sufficiente nel caso di cella estremamente inclinata (vedi dopo).

In tabella \ref{tab:misure effettuate} si trova la lista di tutte le misure effettuate; lo spazio dei parametri finora ``esplorato'' è invece rappresentato nel grafico in figura \ref{fig:grafico spazio dei parametri esplorato}.

\begin{figure}[b!]
\begin{center}\includegraphics[height=9cm]{paramspace}\end{center}
    \caption{Spazio dei parametri finora ``esplorato''; ogni punto corrisponde ad una misura. L'inclinazione è espressa in {\milli\meter} di sollevamento del supporto, $\unita{1}{\milli\meter} \leftrightarrow \unita{6,\overline{6}}{\milli\radian}$.}
\label{fig:grafico spazio dei parametri esplorato}
\end{figure}

\begin{landscape}
\begin{small}
\LTcapwidth=\textwidth
\begin{longtable}{lrrrrrrr|p{7cm}}
\caption{Misure effettuate e tempo a cui si osservano i vari pattern (espresso sempre in numero di frame, da moltiplicare per $\sim 5$ per ottenere il numero di secondi); è importante notare che, salvo il transitorio iniziale, tutti gli altri tempi sono molto indicativi (trattandosi di processi molto graduali c'è una forte componente soggettiva nel decidere quando si è passati ad un altro pattern). Ogni riga orizzontale corrisponde alla sostituzione del campione utilizzato.}
\label{tab:misure effettuate} \\
\toprule
Data & \unita{\Delta z}{(\milli\meter)} & \unita{\Delta T}{(\kelvin)} & Transit. & Drift & Reticolaz & Poligoni & Quadrati & Note \\ 
\midrule
\endfirsthead
\bottomrule
\endfoot
\caption{Misure effettuate con tempo a cui si osservano i vari pattern (continua).} \\
\toprule
Data & \unita{\Delta z}{(\milli\meter)} & \unita{\Delta T}{(\kelvin)} & Transit. & Drift & Reticolaz & Poligoni & Quadrati & Note \\
\midrule
\endhead
2013-09-19 & 2,50 & 10,86 & 100 & 600 & 220 & 700-1000 & 8000-10000 &  \\ 
2013-09-21 & 2,80 & 10,86 & 104 & 700 & 500 & 1000 & 1500-3000 & Il pattern diventa particolarmente chiaro e stabile attorno ai 7000-8000. \\ 
2013-09-23 & 3,50 & 10,86 & 108 & 600 & 500 & 700-1100 & 2000?-? & Dataset ``sporco'' e troppo breve per vedere cosa sarebbe accaduto dopo. \\ 
2013-09-23 & 3,20 & 10,86 & 101 & 800 & 240 & 700 & 2700-5200 &  \\ 
2013-09-24 & 5,00 & 10,86 & 110 & 700 & 230 & 1000 & 8000-12000 &  \\ 
2013-09-25 & 3,00 & 10,86 & 100 & 300 & 300 & 530 & 1500 &  \\ 
2013-09-26 & 0,00 & 10,86 & 95 & / & 450 & 600 & 4000? & In generale nei dataset piatti i quadrati non si allineano mai bene, resta sempre un po' tutto disordinato. \\ 
2013-09-27 & 0,00 & 4,93 & 195 & / & 750 & 1100 & 1500? &  \\ 
2013-09-29 & 0,00 & 2,47 & 420 & / & 1600 & 2100 &  & Si vede molto poco. \\ 
2013-09-30 & 0,00 & 1,24 & ? & ? & ? & ? & ? & Non si vede nulla, poi da 5104 convezione ed ebollizione causa problemi al termostato. \\ 
\midrule
2013-10-01 & 0,00 & 1,15 & ? & ? & ? & ? & ? & Non si vede nulla. \\ 
2013-10-03 & 0,00 & 2,14 & 300 & 1200 & 1000 &  &  & Dataset non affidabile causa bolla d'aria. \\ 
2013-10-04 & 0,00 & 1,63 & ? &  & 2000? & 2000? &  & Non si vede nulla. \\ 
\midrule
2013-10-05 & 2,80 & 4,37 & 186 & 600 & 800 & 1000 & 3000-6000 & A $\sim 1000$ aumento di temperatura; per il resto, particolarmente ben riuscito. \\ 
2013-10-06 & 2,80 & 2,50 & 260 & 600 & 900 & 1300 & 3000-5000 & Molto chiaro. \\ 
2013-10-07 & 2,80 & 5,18 & 140 & 300 & 350 & 500 & 1500-5000 & Molto chiaro. \\ 
2013-10-08 & 2,80 & 7,10 & 125 & 700 & 300 & 320 & 1500-6000 &  \\ 
2013-10-09 & 10,00 & 7,37 & 140 & / & / & / & / & Transitorio a vermoni, lentamente si accumulano pattern reticolari ai bordi (all'incrocio con gli estremi dell'asse di inclinazione). \\ 
2013-10-09 & 10,00 & 7,35 & 130 & / & / & 10000 &  & Partito ½h dopo il precedente; vermoni, poi reticola man mano dai bordi c.s.; attorno a 10000 è tutto pieno. \\ 
2013-10-11 & 10,00 & 4,99 & 155 & 400 & 700 & 1000 & / & Dataset lunghissimo (\unita{\sim 130000}{\second}), solo vermoni. \\ 
2013-10-13 & 10,00 & 7,71 & 140 & 300 & 300 & 4000-8000 &  & Tende a reticolare subito sui bordi. \\ 
2013-10-15 & 10,00 & 7,62 & 135 & 200 & 1000 & 1300 &  & Prima reticolazione vermonica attorno ai 200 (prima i bianchi), poi sequenza normale. \\ 
\midrule
2013-10-16 & 10,00 & 7,06 & 120 & / & / & / & / & Solo vermoni. \\ 
2013-10-17 & 10,00 & 7,02 & / & / & / & / & / & Continuazione del precedente, sempre solo vermoni. \\ 
2013-10-19 & 10,00 & 5,15 & 140 & 400 & 450 & 1500-2000 &  & Temperatura particolarmente instabile; prima reticolazione vermonica a $\sim 400$. \\ 
2013-10-21 & 10,00 & 9,60 &  &  & &  &  & Numerosi cambiamenti di temperatura (all'inizio e alla fine – rottura peltier), accenni di reticolazione, poi vermoni dall'aumento di temperatura. \\ 
2013-10-24 & 10,00 & 6,80 & 140 & 400 & 400 & ?-4000 &  & Problemi per connettore difettato termistore. \\ 
2013-10-25 & 10,00 & (varie) & / & / & / & / &  & Primo test convezione. \\ 
2013-10-26 & 10,00 & 4,31 & 180 & 1100 & 350 & 2000 &  &  \\ 
2013-10-28 & 10,00 & 2,57 & 315 & 600 & 1000? & 2000? &  & Sostanzialmente non si vede nulla. \\ 
\end{longtable}
\end{small}
\end{landscape}

\section{Fenomenologia}

\subsection{Fasi comuni a tutte le condizioni di misura}

La fenomenologia osservata possiede caratteristiche comuni a tutte le temperature e inclinazioni; in particolare, normalmente il campione passa attraverso le seguenti fasi:

\paragraph*{Transitorio iniziale} Ad un tempo approssimativamente proporzionale a $\Delta T^{-3/4}$ (vedi grafico in figura \ref{fig:grafico tempo transitorio}) il sistema passa per un transitorio caratterizzato da colonne scure e chiare che compaiono in tutta la cella (partendo da lati opposti nel caso di cella inclinata). Questo corrisponde alla prima oscillazione di intensità nel grafico del contrasto (prima immagine in figura \ref{fig:fasi misure inclinazione intermedia}). In particolare, normalmente compaiono per prime le colonne bianche (picco), quindi le nere (metà discesa). La situazione mostrata in figura corrisponde a circa metà della prima oscillazione in figura \ref{fig:grafico contrasto misure inclinazione intermedia}.

\begin{figure}[b!]
\includegraphics[width=\textwidth]{graficotempotransitorio}
\caption{Andamento del tempo a cui si presenta il transitorio in funzione della temperatura.}
\label{fig:grafico tempo transitorio}
\end{figure}


\paragraph*{Reticolazione e drift} Dopo il transitorio iniziale, il contrasto diminuisce leggermente e, dopo una fase abbastanza confusa, inizia la reticolazione, in cui si hanno accenni di pattern poligonali; inizia a diventare evidente anche l'eventuale drift dei pattern indotto dall'inclinazione della cella. In genere questo corrisponde all'ultimo picco prima del plateau nel grafico del contrasto.

\paragraph*{Pattern poligonali} Al termine della reticolazione si ottiene un pattern in genere esagonale (più o meno regolare) abbastanza stabile, che, nel caso di cella inclinata, scorre lungo l'asse di inclinazione della cella (i pattern bianchi in un verso, quelli scuri nell'altro). I tempi indicati in tabella \ref{tab:misure effettuate} in proposito sono solo indicativi, dato che il processo è molto graduale e la scelta dell'esatto momento in cui si può dire che i pattern sono formati è estremamente soggettiva.

In generale, comunque, la rapidità di formazione dei pattern sembra essere influenzata positivamente dalla differenza di temperatura e negativamente dall'inclinazione del campione. Ad inclinazioni elevate (ad esempio a \unita{10}{\milli\meter}), infatti, tende a prevalere il moto su grande scala, che in diversi casi impedisce completamente la formazione di pattern (vedi dopo).

\subsection{Caratteristiche specifiche di misure ad inclinazione intermedia}

\paragraph*{Pattern rettangolari} In misure ad inclinazione intermedia si assiste ad un graduale cambiamento del pattern: ad un tempo compreso tra i $7500$ e i \unita{15000}{\second} i pattern, da esagonali, tendono a diventare quadrati/rettangolari, allineati rispetto all'asse di inclinazione della cella; è chiaro quindi che l'inclinazione della cella tende ad avere un'influenza notevole anche sulla forma dei pattern oltre che sul drift.\footnote{Accenni di pattern a quadrilateri si vedono di tanto in tanto anche nei dataset ad inclinazione nulla, ma mai ben definiti ed allineati come nei dataset inclinati.}

Tutte le fasi descritte finora mostrate illustrate in figura \ref{fig:fasi misure inclinazione intermedia}; il corrispondente andamento del contrasto è riportato in figura \ref{fig:grafico contrasto misure inclinazione intermedia}.

\begin{figure}
\includegraphics[width=\textwidth/21*10]{1 - transitorio}
\includegraphics[width=\textwidth/21*10]{2 - reticolazione} \\ \\
\includegraphics[width=\textwidth/21*10]{3 - poligonali}
\includegraphics[width=\textwidth/21*10]{4 - quadrati}
\caption{Pattern osservati per un campione inclinato (\unita{2,8}{\milli\meter}) a media differenza di temperatura (\unita{5,18}{\kelvin}); da sinistra a destra, dall'alto in basso: transitorio iniziale, inizio reticolazione/drift, pattern poligonali, pattern quadrati. Tutte le immagini sono ottenute sottraendo il background ai dati ottenuti dal CCD, aggiungendo 127 alla luminosità di ciascun pixel (il colore grigio di sfondo quindi corrisponde a zone uguali al background) e aumentando il contrasto per maggiore visibilità. Si veda figura \ref{fig:grafico contrasto misure inclinazione intermedia} per il grafico del contrasto.}
\label{fig:fasi misure inclinazione intermedia}
\end{figure}

\begin{figure}
\includegraphics[height=9cm,width=\textwidth]{contrastgraph}
\includegraphics[height=9cm,width=\textwidth]{contrastgraph-detail}
\caption{Andamento di temperatura e contrasto nel dataset di figura \ref{fig:fasi misure inclinazione intermedia} (vista globale e dettaglio della prima ora); nella seconda immagine, il primo picco corrisponde all'inizio del transitorio, l'ultimo all'inizio della reticolazione. La formazione completa dei primi pattern avviene al rilassamento del sistema sullo stato di contrasto stabile, mentre il passaggio a pattern quadrati non dà segnali particolari su questo grafico. Lo spike che si nota a circa un quarto del primo grafico corrisponde all'accensione della luce in laboratorio, e dunque ovviamente non è rilevante.}
\label{fig:grafico contrasto misure inclinazione intermedia}
\end{figure}

\subsection{Caratteristiche specifiche di misure ad inclinazione elevata}

Le misure ad inclinazione elevata (\unita{\approx 10}{\milli\meter}/\unita{3,8}{\degree}) presentano le peculiarità maggiori, indicazione di prevalenza del moto convettivo su grande scala rispetto alle piccole celle convettive (in drift) evidenziate dalle altre misure.

In generale, tutte le misure ad elevata inclinazione presentano in qualche fase i cosiddetti ``vermoni''\footnote{A cui prima o poi dovrò trovare un nome più presentabile.}.

In alcuni casi essi persistono per tutta la durata dell'esperimento, per cui si ha completa prevalenza del moto su grande scala. In altre misure, essi compaiono nella parte centrale della cella durante il transitorio (quasi fossero i puntini del transitorio ``normale'' allungati), ma agli estremi dell'asse di inclinazione man mano il campione riesce a reticolare (figura \ref{fig:fasi misure vermoniche reticolate}) e talvolta a riempire tutta la cella (in genere in tempi più lunghi rispetto a campioni in condizioni analoghe ma ad inclinazione inferiore). Da notare comunque che i pattern che si formano tendono ad essere meno regolari rispetto ai corrispettivi ad inclinazione minore, e non si ottengono mai pattern quadrati.

\begin{figure}
\includegraphics[width=\textwidth/21*10]{1 - transitorio vermonico ~130}
\includegraphics[width=\textwidth/21*10]{2 - reticolazione vermonica ~10000} \\
\caption{Pattern osservati per un campione fortemente inclinato (\unita{10}{\milli\meter}) a differenza di temperatura medio-alta (\unita{7,37}{\kelvin}). Rispetto ai dataset ad inclinazione media, si nota un transitorio (a $\sim 11$ minuti) non più a ``puntini'', ma a vermoni; il secondo frame mostra lo stato del sistema a $\sim 14$ ore: il pattern è meno definito rispetto alle misure ad inclinazione minore, ma è comunque riuscito ad espandersi in maniera significativa dagli estremi dell'asse di inclinazione verso il centro della cella.}
\label{fig:fasi misure vermoniche reticolate}
\end{figure}

Dall'esame dei dataset acquisiti, la prevalenza dei moti su grande scala sulla reticolazione tende ad essere legata alla elevata differenza di temperatura e forse all'``età'' del campione; i campioni nuovi (o comunque appena posti ad inclinazione elevata) sembrano avere una maggiore tendenza a non reticolare.

La dipendenza dalla temperatura sembra essere confermata sia dalle prese dati a diverse temperature, sia da un dataset in cui si è impostata inizialmente\footnote{Per sbaglio, ma questo non lo diremo.} una temperatura inferiore per poi aumentarla: se qui all'inizio il campione ha iniziato a reticolare, subito all'aumentare della temperatura i moti su grande scala hanno ``smontato'' il reticolo, e fino alla fine del dataset (osservato per circa 60 ore attendendo eventuali cambiamenti) si sono notati solo vermoni.

Viceversa, in dataset a media temperatura (\unita{5}{\kelvin}) ma con un overshoot iniziale, è capitato di notare una prima ``reticolazione vermonica'' in fase di overshoot, che man mano si smonta con lo stabilizzarsi della temperatura; il sistema prosegue poi con le normali fasi descritte nelle sezioni precedenti per i dataset ad inclinazione media (senza però realizzare reticoli quadrati).

Quanto alla dipendenza dall'``età'' del campione, si può pensare che la soluzione, lasciata riposare in posizione inclinata, tenda a sedimentare in maniera asimmetrica (le sferette vetrose nella zona più bassa della cella, l'acqua nella zona più alta), con l'effetto di avere una distribuzione interna analoga a quella di una cella meno inclinata.\footnote{Verificare se sto dicendo cavolate cosmiche.}

\end{document}
