set term push
set terminal postscript eps enhanced color font 'Helvetica,15'
f='ptlist.tsv'
set grid
set xlabel "{/Symbol D}T (K)"
set ylabel "Inclinazione (mm)"
set yrange [-1:12]
set xrange [*:*]
unset key
set output 'paramspace.eps'
plot f using 2:1 ps 1 pt 7 title ''
unset output
unset grid
set term pop
reset
