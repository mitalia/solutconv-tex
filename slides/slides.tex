\documentclass[10pt, xcolor=x11names]{beamer}
\usepackage[utf8x]{inputenc}
\usepackage{ucs}
\usepackage[T1]{fontenc}
\usetheme{Boadilla}
\usecolortheme{whale}
%\usepackage[bookmarks,pdfborder={0 0 0}]{hyperref}
\usepackage{eulervm}
\usepackage[italian]{babel}
\usepackage{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{wasysym}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{icomma}
\usepackage{cancel}
\usepackage{multimedia}
\usepackage{natbib}
\usepackage{appendixnumberbeamer} 
\usepackage[space]{grffile}
\usepackage{siunitx}
\sisetup{output-decimal-marker = {,}}
\sisetup{detect-all}
\usepackage[version=3]{mhchem}
\usepackage[scaled=.7]{DejaVuSansMono}
\usepackage{MnSymbol}
\usepackage{listings}
\lstset{
basicstyle=\small\ttfamily,
numbers=left,
numberstyle=\scriptsize,
frame=tbp,
columns=fullflexible,
showstringspaces=false,
breaklines=true
}
\lstset{prebreak=\raisebox{0ex}[0ex][0ex]
        {\ensuremath{\rhookswarrow}}}
\lstset{postbreak=\raisebox{0ex}[0ex][0ex]
        {\ensuremath{\rcurvearrowse\space}}}
\renewcommand\lstlistlistingname{Listati}
\renewcommand\lstlistingname{Listato}
\renewcommand{\footnotesize}{\tiny} 
\newcommand{\Ra}{\operatorname{Ra}}
\newcommand{\diffd}{\operatorname{d}}
\newcommand{\slfrac}[2]{\left.#1\middle/#2\right.}
\DeclareMathOperator{\sgn}{sgn}
\graphicspath{{../images/}}
\author[Matteo Italia]{Matteo Italia \vspace{2mm} \\ {\scriptsize{Relatore: Prof. Alberto Vailati}} \\ {\scriptsize{Correlatore: Dott. Fabrizio Croccolo}}}
\title[]{Studio sperimentale di moti convettivi in un nanofluido in condizioni di strato limite inclinato}
\institute[]{Università degli Studi di Milano}
\date{16 dicembre 2013}
\begin{document}
\begin{frame}{\centerline{\normalsize Tesi di Laurea Triennale in Fisica}}

\maketitle
\end{frame}
\section{Cenni teorici}

\begin{frame}{Convezione di Rayleigh-Bénard}

    \textit{Fluido semplice} in cella sottile, orizzontale, faccia inferiore riscaldata ($T_\text{inf} > T_\text{sup}$)

    \begin{columns}[t]
        \begin{column}{\textwidth/22*10}
            \begin{block}{Numero di Rayleigh}

                \[
                    \Ra = \frac{\alpha g \Delta T d^3}{\chi \nu}
                \]
                {\scriptsize ($\alpha=-1/\rho(\partial\rho/\partial T)$ coefficiente di espansione termica volumetrica, $g$ accelerazione di gravità, $d$ spessore della cella, $\Delta T$ differenza di temperatura, $\nu$ viscosità cinematica )}

                \begin{itemize}
                    \item \textbf{grandezze rilevanti ai fini della convezione condensate in un numero adimensionale}
                    \item soglia di convezione: $\Ra > \Ra_c$.
                \end{itemize}
            \end{block}
        \end{column}
        \begin{column}{\textwidth/22*10}
            \adjincludegraphics[width=\textwidth, valign=t]{dettaglio RB}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Fluido binario: convezione solutale}

    \begin{columns}[t]
        \begin{column}{\textwidth/22*12}
            \begin{block}{}
                \begin{itemize}
                    \item \textbf{Effetto Soret} \\ $\vec \nabla T \Rightarrow \vec \nabla c \Rightarrow \vec \nabla \rho \Rightarrow $ \textbf{convezione}
                    \item coefficiente di Soret ($S_T$): \textbf{intensità e verso dell'effetto} 
                    \item \textbf{convezione riscaldando dall'alto} per $S_T < 0$ (componente densa termofila)
                    %\item RB e convezione solutale agiscono insieme, intensità e verso relativi riassunti nel \textit{rapporto di separazione} $\Psi = ({\beta}/{\alpha}) S_T c(1-c) $ (con $ \beta=\rho^{-1} \frac{ \partial \rho }{ \partial c}$)
                    %\item con RB soppresso ($\Psi \ll -1$), \textbf{la convezione solutale è formalmente identica a RB} (con $ \Ra_s = \frac{\beta g S_T c(1-c) \Delta T d^3}{\nu D} $ al posto di $\Ra$)  %($D$ coefficiente di diffusione termica)
                \end{itemize}
            \end{block}
        \end{column}
        \begin{column}{\textwidth/22*8}
            \adjincludegraphics[width=\textwidth, valign=t]{soret}
        \end{column}
    \end{columns}
        
\end{frame}

\begin{frame}{Effetto dell'inclinazione}

    \begin{block}{}
        \begin{center}
            \includegraphics[width=\textwidth/10*7]{cella inclinata}
        \end{center}
        \begin{itemize}
            \item rottura di simmetria, \textbf{direzione preferenziale}
            \item \textbf{scorrimento boundary layer} (BL denso scende, BL leggero sale)
            \item \textbf{flusso su grande scala} (\textit{LSF}), concorrenza con colonne delle celle convettive
            \item instabilità convettiva su grande scala parte dall'accumulo di materiale ai bordi
        \end{itemize}

    \end{block}

\end{frame}


\begin{frame}{Studi precedenti e scopo del lavoro}


    \begin{columns}[t]
        \begin{column}{\textwidth/22*11}
            \begin{block}{}
                Convezione inclinata \textit{a $S_T>0$} in precedenti lavori\footnotemark \textrightarrow ``superhighway convection'' (tempi lunghi)
            \end{block}
            \begin{alertblock}{Scopo di questo lavoro}
                Osservare la \textbf{\textit{formazione} e l'\textit{evoluzione} delle strutture convettive} nel campione in \textbf{convezione solutale inclinata} a $S_T<0$.
            \end{alertblock}
        \end{column}
        \begin{column}{\textwidth/22*9}
            \adjincludegraphics[width=\textwidth,valign=t]{SHC}
        \end{column}
    \end{columns}
    \footnotetext{
        F. Croccolo, F. Scheffold, and A. Vailati,
        \textit{Effect of a Marginal Inclination on Pattern Formation in a Binary Liquid Mixture under Thermal Stress},
        Phys. Rev. Lett.,
        \textbf{111},
        014502 (2013)
        }
\end{frame}

\section{Apparato sperimentale}

\begin{frame}{Ottica per shadowgraph}

    \begin{columns}[t]
        \begin{column}{\textwidth/22*7}
            \adjincludegraphics[height=7.5cm,valign=t]{banco ottico}
        \end{column}

        \begin{column}{\textwidth/22*13}
            \adjincludegraphics[width=\textwidth, valign=t]{shadowgraph}
            \begin{block}{}
                \begin{itemize}
                    \item Estremanti di indice di rifrazione $\approx$ lenti $\Rightarrow$ zone chiare o scure
                    \item Indice di rifrazione dipende da temperatura o \textit{concentrazione di soluto} \textrightarrow si rendono \textbf{visibili i flussi convettivi}.
                \end{itemize}
            \end{block}
        \end{column}

    \end{columns}

\end{frame}

\begin{frame}{Cella e supporto}

    \includegraphics[width=\textwidth]{schema cella}

\end{frame}

\begin{frame}{Riepilogo elettronica/software di controllo}
    \includegraphics[width=\textwidth]{riepilogo funzionamento strumento}
\end{frame}

\begin{frame}{Software}{Termostatazione: il controller PID}
    \[
        o(t) = K_p \cdot e(t) + K_i \cdot \int_{0}^{t} e(t') \diffd t' + K_d \cdot \frac{\diffd e}{\diffd t}(t)
        \label{eq:controller PID}
    \]

    \begin{block}{}
        \begin{itemize}
            \item \textbf{``errore''}: $e(t) = f(t)-s$ \\ ($f(t)$ temperatura rilevata, $s$ temperatura target)
            \item \textbf{output}: $o(t)$ (corrente erogata sui Peltier)
        \end{itemize}
    \end{block}
    \begin{block}{Termini}
        \begin{itemize}
            \item \textit{proporzionale} ($K_p$)
            \item \textit{integrale} ($K_i$): corregge il \textit{droop} (errore sistematico residuo e.g. da dispersioni)
            \item \textit{derivativo} ($K_d$): reazione rapida a cambiamenti repentini di condizioni esterne
        \end{itemize}
    \end{block}
\end{frame}

\section{Risultati}

\begin{frame}{Condizioni sperimentali}

    \begin{columns}[t]

        \begin{column}{\textwidth/22*10}
            \begin{block}{Campione}
                Ludox TMA: sospensione 4\% (m/m) di sferette di silice nanometriche ($ \diameter = \SI{20}{\nano\meter}$)
                \begin{itemize}
                    \item $S_T = -0,047$: silice termofila \textrightarrow \textbf{instabilità scaldando dall'alto}
                    \item predomina \textbf{instabilità da effetto Soret}
                    \item indice di rifrazione \textbf{aumenta} con la concentrazione di silice
                \end{itemize}
            \end{block}
        \end{column}

        \begin{column}{\textwidth/22*10}
            \begin{block}{Escursione dei parametri}
                \begin{itemize}
                    \item $1 < \Delta T < 11$ \si{\celsius}
                    \item da cui $10^7 < \Ra_s < 1.2 \cdot 10^8$
                    \item $\vartheta < \SI{70}{\milli\radian} \approx \SI{4}{\degree}$
                \end{itemize}
            \end{block}

            \begin{block}{Tempi caratteristici}
                \begin{itemize}
                    \item tempo diffusivo: $\tau \approx \SI{106}{\hour}$;
                    \item tempo di rilassamento (posto tra una misura e l'altra): $\sim \tau / 10$;
                    \item durata di ciascuna misura: $15 - \SI{25}{\hour}$ 
                \end{itemize}
            \end{block}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}{Interpretazione delle immagini shadow}

    \begin{columns}
        \begin{column}{\textwidth/4*3}
            {\hfill lato basso \hspace*{1cm}}
            \begin{center}
                \movie[poster,width=\textwidth,height=0.75\textwidth]{\includegraphics[width=\textwidth]{interpretazione immagini}}{media/Qmed.mp4}
            \end{center}
            {\hspace*{1cm}lato alto}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Contrasto e transitorio iniziale}
    \textbf{Contrasto} indicatore importante dell'\textbf{intensità di convezione}
        \[
            C(t) = \left< \left[I(t,x,y) - I(t_0,x,y)\right]^2 \right>_{x,y}
        \]

    \begin{columns}[t]
        \begin{column}{\textwidth/44*13}
            \begin{block}{}
                \begin{itemize}
                    \item primo spike: avvio convezione, \textbf{transitorio iniziale}
                    \item chiara dipendenza del tempo di avvio da $\Delta T$
                \end{itemize}
            \end{block}
        \end{column}
        \begin{column}{\textwidth/44*27}
            \adjustbox{valign=t}{
                \includegraphics[width=\textwidth, page=2]{contrastvsT}
                }
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}{Transitorio iniziale}{Confronto tempi}

    \begin{columns}[t]
        \begin{column}{\textwidth/44*21}
            \begin{block}{\textbf{Power law}: $ t \propto (\Delta T)^{-\alpha} $}

                \adjincludegraphics[width=\textwidth, valign=t]{graficotempotransitorio_slides}
            \end{block}
        \end{column}
        \begin{column}{\textwidth/44*21}
            \begin{block}{Confronto con dati di letteratura\footnotemark }

                \adjincludegraphics[width=\textwidth, valign=t]{confronto onset convezione con cerbino2005_slides}
            \end{block}
        \end{column}
    \end{columns}
    \footnotetext{R. Cerbino, S. Mazzoni, A. Vailati, and M. Giglio, \textit{Scaling Behavior for the Onset of Convection in a Colloidal Suspension}, Phys. Rev. Lett., \textbf{94}, 064501 (2005)}
\end{frame}

\begin{frame}{Transitorio iniziale}{Influenza dell'inclinazione}

    \begin{columns}[t]

        \begin{column}{\textwidth/44*21}
            \begin{block}{Bassa inclinazione ($\vartheta \apprle \SI{50}{\milli\radian}$)}
                \begin{center}
                    \adjustbox{valign=t}{
                        \movie[poster,width=\textwidth,height=0.75\textwidth]{\includegraphics[width=\textwidth]{transitorio/3}}{media/Qtrans.mp4}
                        }
                    {\scriptsize $\vartheta = \SI{18,67}{\milli\radian}$, $\Delta T = \SI{5,18}{\celsius}$, $\SI{2,1}{\minute\per\second}$}
                \end{center}
            \end{block}
        \end{column}

        \begin{column}{\textwidth/44*21}
            \begin{block}{Alta inclinazione ($\vartheta \apprge \SI{50}{\milli\radian}$)}
                \begin{center}
                    \adjustbox{valign=t}{
                        \movie[poster,width=\textwidth,height=0.75\textwidth]{\includegraphics[width=\textwidth]{transitorio vermonico ibrido/3}}{media/VPtrans.mp4}
                        }
                    {\scriptsize $\vartheta = \SI{66,67}{\milli\radian}$, $\Delta T = \SI{7,71}{\celsius}$, $\SI{2,1}{\minute\per\second}$}
                \end{center}
            \end{block}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}{Misure a media e bassa inclinazione ($\vartheta \apprle \SI{50}{\milli\radian}$)}

    \begin{columns}[t]
        \begin{column}{\textwidth/22*7}
            \begin{block}{}
                Seguono:
                \begin{itemize}
                    \item \textbf{drift} (per misure inclinate);
                    \item \textbf{spoke pattern} poligonali;
                    \item spoke pattern quadrati \textbf{allineati con l'asse di inclinazione} (misure inclinate).
                \end{itemize}
            \end{block}
        \end{column}
        \begin{column}{\textwidth/22*13}
            \adjustbox{valign=t}{
            \movie[poster,width=\textwidth,height=0.75\textwidth]{\includegraphics[width=\textwidth]{4 - quadrati}}{media/Qfast.mp4}}

            \begin{block}{}
                $\vartheta = \SI{18,67}{\milli\radian}$, $\Delta T = \SI{5,18}{\celsius}$, $\SI{21,6}{\minute\per\second}$
            \end{block}
        \end{column}
    \end{columns}

\end{frame}

\begin{frame}{Misure ad elevata inclinazione ($\vartheta \apprge \SI{50}{\milli\radian}$) }{Fenomenologia}

    \begin{columns}[t]
        \begin{column}{\textwidth/22*7}
            \begin{block}{}
                \begin{itemize}
                    \item<1-> spesso \textbf{prevalenza completa del flusso su grande scala};
                    \item<2-> eventuale spoke pattern da zona alta/bassa della cella
                \end{itemize}
            \end{block}
        \end{column}
        \begin{column}{\textwidth/22*13}
            \only<1>{
                \adjustbox{valign=t}{
                \movie[poster,width=\textwidth,height=0.75\textwidth]{\includegraphics[width=\textwidth]{transitorio vermonico puro/4}}{media/Vfast.mp4}}
                \begin{block}{}
                    $\vartheta = \SI{66,67}{\milli\radian}$, $\Delta T = \SI{7,06}{\celsius}$, $\SI{21,6}{\minute\per\second}$
                \end{block}
            }
            \only<2>{
                \adjustbox{valign=t}{
                \movie[poster,width=\textwidth,height=0.75\textwidth]{\includegraphics[width=\textwidth]{1 - transitorio vermonico ~130}}{media/VPfast.mp4}}
                \begin{block}{}
                    $\vartheta = \SI{66,67}{\milli\radian}$, $\Delta T = \SI{7,71}{\celsius}$, $\SI{21,6}{\minute\per\second}$
                \end{block}
            }
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Schema complessivo misure e conclusioni}

    \begin{columns}[t]
        \begin{column}{\textwidth/22*7}
            \begin{block}{}
                \begin{itemize}
                    \item realizzazione strumento per shadowgraph
                    \item confronto con lavori precedenti (SHC a $S_T>0$, tempi di avvio convezione)
                    \item \textbf{studio sistematico} dipendenza da $\Delta T$ e $\vartheta$
                \end{itemize}
            \end{block}
        \end{column}
        \begin{column}{\textwidth/22*13}
            \adjincludegraphics[height=7.6cm,valign=t,right]{phasediagram_slides}
        \end{column}
    \end{columns}
\end{frame}

\appendix

\begin{frame}{Diapositive extra}

\end{frame}

\begin{frame}{Software}

    \begin{columns}[t]
        \begin{column}{\textwidth/22*10}
            \begin{block}{Compiti del software}
                \begin{itemize}
                    \item termostatazione (controller PID)
                    \item safeguard (controllo ridondante temperature)
                    \item acquisizione immagini (1 frame ogni \SI{5}{\second}, salvataggio differenza con background)
                    \item analisi batch immagini (report contrasto e andamento temperatura)
                \end{itemize}
            \end{block}
        \end{column}

        \begin{column}{\textwidth/22*10}
            \begin{block}{Hardware da controllare}
                \begin{itemize}
                    \item due multimetri 
                    \item due alimentatori 
                    \item scheda NI 4350 
                    \item framegrabber
                \end{itemize}
            \end{block}

            \begin{alertblock}{Linguaggio impiegato: Python}
                \begin{itemize}
                    \item rapidità sviluppo
                    \item facilità di interfacciamento hardware
                    \item gestione immagini 
                    \item ampia libreria scientifica
                    \item non necessarie alte prestazioni
                \end{itemize}
            \end{alertblock}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Misure ad elevata inclinazione ($\vartheta \apprge \SI{50}{\milli\radian}$)}{Confronto con convezione con scorrimento}

    \begin{columns}[t]
        \begin{column}{\textwidth/22*6}
            \begin{block}{}
                \begin{itemize}
                    \item Somiglianza con pattern da studi su scorrimento della crosta terrestre sul mantello\footnotemark.
                    \item Punto in comune: \textbf{scorrimento relativo} dei BL rispetto al resto del fluido \textrightarrow \textbf{sforzo tangenziale}.
                \end{itemize}
            \end{block}
        \end{column}
        \begin{column}{\textwidth/22*14}
            \\

            \adjincludegraphics[width=\textwidth/32*10,valign=t]{mckenzie/1}
            \adjincludegraphics[width=\textwidth/32*10,valign=t]{mckenzie/2}
            \adjincludegraphics[width=\textwidth/32*10,valign=t]{mckenzie/3}
            \adjincludegraphics[width=\textwidth/32*10,valign=t]{mckenzie/4}
            \adjincludegraphics[width=\textwidth/32*10,valign=t]{mckenzie/5}
            \adjincludegraphics[width=\textwidth/32*10,valign=t]{mckenzie/6}
        \end{column}
    \end{columns}

    \footnotetext{
        D. P. McKenzie, F. Richter,
        \textit{Convection Currents in the Earth's Mantle},
        Scientific American,
        \textbf{235}, p. 72-89
        (1976)}

\end{frame}

\begin{frame}{Classificazione dei tipi di misure}
    \begin{columns}[t]
        \begin{column}{\textwidth/22*6}
            \begin{block}{}
                \begin{itemize}
                    \item Classificazione per \textbf{``tipi di evoluzione''} del sistema; 
                    \item scopo: suddividere i dati raccolti per \textbf{costruire un diagramma di fase}.
                \end{itemize}
            \end{block}
        \end{column}
        \begin{column}{\textwidth/22*14}
            \adjincludegraphics[width=\textwidth,valign=t]{diagramma di flusso fenomenologia}
        \end{column}
    \end{columns}
\end{frame}


\end{document}
