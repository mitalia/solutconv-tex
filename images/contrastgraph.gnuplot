set term push
set terminal postscript eps enhanced mono font 'Helvetica,15'
f='2013-10-06 - 1 - 2.8 mm - fake7 C (5).tsv'
set xlabel "Tempo (s)"
set yrange [0:50]
set xrange [*:*]
set output 'contrastgraph.eps'
plot f using 1:2 with lines title 'Contrasto (a.u.)', f using 1:3 with lines title '{/Symbol D}T (K)'
set output 'contrastgraph-detail.eps'
set xrange [0:3600]
plot f using 1:2 with lines title 'Contrasto (a.u.)', f using 1:3 with lines title '{/Symbol D}T (K)'
set term pop
reset
