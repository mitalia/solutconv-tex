#/bin/bash
set -u
(
cat <<EOF
set term push
set terminal pdf enhanced color font 'Helvetica,15' size $2cm,$3cm
set grid
set yrange [-5:75]
set xrange [0:1.3e+08]
set xlabel "Ra_s"
#set x2label "\Delta T"
#set x2tics 1
set ylabel "Inclinazione (mrad)"
#set y2label "Elevazione (mm)"
#set y2tics 1
set output '$1'
set key outside center bottom horizontal
EOF
) > phasediagram.gnuplot

idx=0
declare -a shapes=(5   13   11   9   6   12)
declare -a names=('Q' 'PQ' 'VP' 'V' 'NA' 'P')
declare -a colors=( '#004586' '#ff420e' '#579d1c' '#7e0021' '#314004' '#aecf00' '#4b1f6f' '#ff950e' '#c5000b' '#0084d1')
for ((idx=0 ; idx < ${#names[@]} ; idx++))
do
    if [ $idx -eq 0 ]
    then
        echo -n "plot ">>phasediagram.gnuplot
    else
        echo -n ", ">>phasediagram.gnuplot
    fi
    t=${names[$idx]}
    out=phasediagram-$t.tsv
    grep -P "\t$t\t" misure.tsv > $out
    echo -n " '$out' using 7:3 ps 1 pt ${shapes[$idx]} linecolor rgb '${colors[$idx]}' title '$t' axes x1y1 " >> phasediagram.gnuplot
    #echo -n ", '$out' using 5:2 pt -1 axes x2y2 title '' " >> phasediagram.gnuplot
done
gnuplot phasediagram.gnuplot
cp "$1" ../images
