#/bin/bash
(
cat <<'EOF'
set term push
set terminal pdf enhanced color font 'Helvetica,15' size 13cm,9cm
set grid
set yrange [10:60]
set xrange [0:50000]
set xlabel "t (s)"
set ylabel "Contrasto (a.u.)"
set output 'contrastvsT.pdf'
#set key outside center bottom horizontal
EOF
) > contrastvsT.gnuplot

declare -a colors=( '#004586' '#ff420e' '#579d1c' '#7e0021' '#314004' '#aecf00' '#4b1f6f' '#ff950e' '#c5000b' '#0084d1')
cd contrastvsT
for ((c=0 ; c<2 ; c++))
do
    idx=0
    for i in $(find *.tsv | sort -rg)
    do
        if [ $idx -eq 0 ]
        then
            echo -n "plot ">>../contrastvsT.gnuplot
        else
            echo -n ", ">>../contrastvsT.gnuplot
        fi
        t=$(basename "$i" '.tsv')
        echo -n " 'contrastvsT/$i' using 1:2 lw 3 linecolor rgb'${colors[$idx]}$' title '$t °C' with lines " >> ../contrastvsT.gnuplot
        idx=`expr $idx + 1`
    done
    echo " " >> ../contrastvsT.gnuplot
    echo "set xrange [0:3500]" >> ../contrastvsT.gnuplot
done
cd ..
gnuplot contrastvsT.gnuplot
cp contrastvsT.pdf ../images
